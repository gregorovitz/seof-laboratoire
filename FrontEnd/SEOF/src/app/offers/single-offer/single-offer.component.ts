
import { Component, OnInit } from '@angular/core';
import { Offer } from '../../models/offer.model';
import { Router } from '@angular/router';
import { OffersService } from '../../services';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-single-offer',
  templateUrl: './single-offer.component.html',
  styleUrls: ['./single-offer.component.scss']
})
export class SingleOfferComponent implements OnInit {

  FormContact: FormGroup;
  href: string = "";
  id:number;
  offers:Offer

  constructor(private formBuilder: FormBuilder,
              private offersService: OffersService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
    this.href = this.router.url;
    this.id=parseInt(this.href.replace('/single-offer/',''))
    this.offersService.getOne(this.id) 
      .subscribe((offer:Offer)=>{
        this.offers=offer;
        console.log(this.offers)
      })
  }

  initForm() {
    this.FormContact = this.formBuilder.group({
      message: ['', [Validators.required]]
    });
  }

  onSubmit(mail,subject,date){
    let body=this.FormContact.value.message
    window.location.href = "mailto:"+mail +"?body="+body+"&subject=mail de SEOF concernant votre offre "+subject+ " pour le : "+ date;
  }
}
