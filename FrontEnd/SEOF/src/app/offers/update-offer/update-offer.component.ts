import { OffersService,CityService,CategorieService,AlertService } from './../../services';
import { Component, OnInit } from '@angular/core';
import { Offer } from '../../models/offer.model';
import {  Router } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-offer',
  templateUrl: './update-offer.component.html',
  styleUrls: ['./update-offer.component.scss']
})
export class UpdateOfferComponent implements OnInit {
  UpdateOfferForm:FormGroup
  FormContact:FormGroup
  href: string = "";
  id:number;
  offers:Offer
  categories=[]
  allCities=[]
  cities=<any>[]
  selected : number;
  constructor(private formBuilder: FormBuilder,
    private citiesService:CityService,
    private categorieService:CategorieService,
    private alertService: AlertService,
    private offersService:OffersService,
    private router:Router
    ) { }

  ngOnInit(): void {
    this.initForm();
    this.href = this.router.url;
    this.id=parseInt(this.href.replace('/user/single-offer/',''))
    this.offersService.getOne(this.id) 
      .subscribe((offer:Offer)=>{
        console.log(offer)
        let x={title:offer.title,description:offer.description,categorie:offer.TypeId,dateBesoin:offer.offer_date,localite:offer.City.name,prix:offer.price,userId:offer.UserId,offreId:offer.id}
        this.UpdateOfferForm.patchValue(x)
      })
    this.categorieService.getAll()
      .subscribe((categorie)=>{
        this.categories=categorie
      })  
      this.UpdateOfferForm.valueChanges.subscribe(
        term => {
          if (term.localite != '') {
            this.cities=filterByValue(this.allCities,term.localite)
          }
      })
    this.citiesService.getAll()
      .subscribe((city)=>{
        this.allCities=city
      }) 
  }
  initForm() {
    this.UpdateOfferForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      description: ['', [Validators.required]],
      categorie: ['', [Validators.required]],
      dateBesoin: ['', [Validators.required]],
      localite: ['', [Validators.required]],
      prix:[''],
      userId:[''],
      offreId:['']
    });
  }
  onSubmit() {
    this.alertService.clear();
    this.offersService.update(this.UpdateOfferForm.value)
    .pipe()
    .subscribe(
        (data:any) => {
          console.log(data.message) 
            if (data.message==="user created"){
              
              this.alertService.success(data.message,true)
          }else{
            this.alertService.error(data.message);
          }
        },
        error => {
          this.alertService.error(error);
          
        });
  }
}
function filterByValue(array, term) {
  return array.filter(o => o.name.toLowerCase().includes(term.toLowerCase()));
}