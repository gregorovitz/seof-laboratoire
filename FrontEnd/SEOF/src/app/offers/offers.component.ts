import { Component, OnInit } from '@angular/core';
import { OffersService ,CategorieService} from '../services';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';



@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {
  categorieForm: FormGroup;
  public carditems = []
  public carditemsFiltred =[]
  public pageSlice =[];
  categories:[]

  constructor(private categorieService:CategorieService,
              private formBuilder: FormBuilder,
              private offersService: OffersService,
              private router: Router) {}

  ngOnInit() {
    this.initForm();
    this.offersService.getAll()
      .subscribe((request:[])=>{console.log(request);
        this.carditems=request;
        this.pageSlice=this.carditems.slice(0, 4);
      })
      this.categorieService.getAll()
      .subscribe((categorie)=>{
        this.categories=categorie
      }) 
      this.categorieForm .valueChanges.subscribe(
        id=> {
         
          if (id.categorie != '') {
            this.carditemsFiltred=filterByValue(this.carditems,id.categorie)
            this.pageSlice=this.carditemsFiltred.slice(0, 4);
          }
      })
  }
  initForm() {
    this.categorieForm = this.formBuilder.group({
      categorie: ['', [Validators.required]]
    });
  }

  OnPageChange(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (0){
      if (endIndex > this.carditems.length) {
        endIndex = this.carditems.length;
      }
    this.pageSlice = this.carditems.slice(startIndex, endIndex);
    }else{
      if (endIndex > this.carditemsFiltred.length) {
        endIndex = this.carditemsFiltred.length;
      }
    this.pageSlice = this.carditemsFiltred.slice(startIndex, endIndex);

    }
  }
  onClick(){
    this.carditemsFiltred=[]
    this.pageSlice=this.carditems.slice(0, 4);
    
  }
  singleOffer(carditemsId : number) {
    this.router.navigate(['/single-offer', carditemsId]);
  }
}
function filterByValue(array, id) {
  return array.filter(o => o.TypeId==id);
}