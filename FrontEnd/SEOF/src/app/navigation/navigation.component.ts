import {Component, OnInit} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from '../models';
import { Router } from '@angular/router';

/**
 * @title Toolbar overview
 */
@Component({
  selector: 'app-navigation',
  templateUrl: 'navigation.component.html',
  styleUrls: ['navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  isAuth: boolean;
  currentUser: User;
  constructor(private authService: AuthService,private router: Router,) {
    this.authService.currentUser.subscribe(x => this.currentUser = x);
   }

  ngOnInit() {

  }

  profileUser(currentUserId : number) {
    this.router.navigate(['/profileUser', currentUserId]);
  }

  onSignOut() {
    this.authService.signOutUser();
    this.router.navigate(['/login']);
  }
}


