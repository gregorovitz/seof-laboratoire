import { Router } from '@angular/router';
import { RequestsService,OffersService,AuthService,UserService } from './../services';
import { Component, OnInit } from '@angular/core';


import { User} from '../models'; 
@Component({
  selector: 'app-profileUser',
  templateUrl: './profileUser.component.html',
  styleUrls: ['./profileUser.component.scss']
})
export class ProfileUserComponent implements OnInit {
  currentUser:User
  request:[]
  offers:[]
  constructor(private authService: AuthService,
              private userService: UserService,
              private offresService:OffersService,
              private requestsService:RequestsService,
              private router:Router) {
                this.authService.currentUser.subscribe(x => this.currentUser = x);
               }

  ngOnInit(): void {

    this.userService.getServiceUser(this.currentUser.id)
      .subscribe((req:any)=>{
        this.request=req.Demandes
        this.offers=req.Offers
      }) 
  }
  deleteoffers(id){
    this.offresService.delete(id)
    .subscribe((req:any)=>{
      console.log(req)
      this.offers=filterByValue(this.offers,req.offer)
    })
  }  
  deleterequest(id){
      this.requestsService.delete(id)
      .subscribe((req:any)=>{
        console.log(req)
        this.request=filterByValue(this.request,req.request)
      }) 
  }
  singleOffer(id){
    this.router.navigate(['user/single-offer', id]);
  }
  singleRequest(id){
    this.router.navigate(['user/single-request', id]);
  }

} 
function filterByValue(array, id) {
  return array.filter(o => o.id!=id);
}