import { RequestsService,CityService,CategorieService,AlertService } from './../../services';
import { Request } from './../../models/request.model';
import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-request',
  templateUrl: './update-request.component.html',
  styleUrls: ['./update-request.component.scss']
})
export class UpdateRequestComponent implements OnInit {
  UpdateRequestForm:FormGroup
  FormContact:FormGroup
  href: string = "";
  id:number;
  request:Request;
  categories=[]
  allCities=[]
  cities=<any>[]
  selected : number;
  constructor(
    private formBuilder: FormBuilder,
    private citiesService:CityService,
    private categorieService:CategorieService,
    private alertService: AlertService,
    private requestService:RequestsService,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.href = this.router.url;
    this.id=parseInt(this.href.replace('/user/single-request/',''))
    this.requestService.getOne(this.id) 
      .subscribe((request:Request)=>{
        let x={title:request.title,description:request.description,categorie:request.TypeId,dateBesoin:request.offer_date,localite:request.City.name,userId:request.UserId,offreId:request.id}
        this.UpdateRequestForm.patchValue(x)
      })
    this.categorieService.getAll()
      .subscribe((categorie)=>{
        this.categories=categorie
      })  
      this.UpdateRequestForm.valueChanges.subscribe(
        term => {
          if (term.localite != '') {
            this.cities=filterByValue(this.allCities,term.localite)
          }
      })
    this.citiesService.getAll()
      .subscribe((city)=>{
        this.allCities=city
      }) 
  }
  initForm() {
    this.UpdateRequestForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      description: ['', [Validators.required]],
      categorie: ['', [Validators.required]],
      dateBesoin: ['', [Validators.required]],
      localite: ['', [Validators.required]],
      userId:[''],
      offreId:['']
    });
  }
  onSubmit() {
    this.alertService.clear();
    this.requestService.update(this.UpdateRequestForm.value)
    .pipe()
    .subscribe(
        (data:any) => {
          console.log(data.message) 
            if (data.message==="user created"){
              this.alertService.success(data.message,true)
          }else{
            this.alertService.error(data.message);
          }
        },
        error => {
          this.alertService.error(error);
        });
      }
}
function filterByValue(array, term) {
  return array.filter(o => o.name.toLowerCase().includes(term.toLowerCase()));
}
