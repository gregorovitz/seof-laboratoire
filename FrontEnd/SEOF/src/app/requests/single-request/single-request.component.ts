import { Component, OnInit } from '@angular/core';
import { Request } from '../../models/request.model';
import {  Router } from '@angular/router';
import { RequestsService } from '../../services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-single-request',
  templateUrl: './single-request.component.html',
  styleUrls: ['./single-request.component.scss']
})
export class SingleRequestComponent implements OnInit {


  FormContact: FormGroup;
  href: string = "";
  id:number;
  request:Request
  constructor(private formBuilder: FormBuilder,
              private router:Router,
              private requestsService: RequestsService) { }
              
              
              
  ngOnInit() {
    this.initForm();
    this.href = this.router.url;
    this.id=parseInt(this.href.replace('/single-request/',''))
    this.requestsService.getOne(this.id)
      .subscribe((request:Request)=>{
        this.request=request;
        console.log(this.request)
      })   
    
  }
  
  initForm() {
    this.FormContact = this.formBuilder.group({
      message: ['', [Validators.required]]
    });
  }

  onSubmit(mail,subject,date){
    let body=this.FormContact.value.message
    window.location.href = "mailto:"+mail +"?body="+body+"&subject=mail de SEOF concernant votre Demande "+subject+ " pour le : "+ date;
  }
}
