import { RequestsService,CategorieService } from './../services';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
 


@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {

  categorieForm: FormGroup;
  public carditems =[]
  public carditemsFiltred =[]
  public pageSlice =[];
  categories:[]

  constructor(private RequestsService: RequestsService, 
              private formBuilder: FormBuilder,
              private router: Router,
              private categorieService:CategorieService,
              ) {}

  ngOnInit() {
  
    this.initForm();
    this.RequestsService.getAll()
      .subscribe((request:[])=>{
        this.carditems=request;
        this.pageSlice=this.carditems.slice(0, 4);
        
      })  
      this.categorieService.getAll()
      .subscribe((categorie)=>{
        this.categories=categorie
      }) 
      this.categorieForm .valueChanges.subscribe(
        id=> {
         
          if (id.categorie != '') {
            this.carditemsFiltred=filterByValue(this.carditems,id.categorie)
            this.pageSlice=this.carditemsFiltred.slice(0, 4);
          }
      })  
    
  }
  initForm() {
    this.categorieForm = this.formBuilder.group({
      categorie: ['', [Validators.required]]
    });
  }

  OnPageChange(event: PageEvent,bool:number) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (0){
      if (endIndex > this.carditems.length) {
        endIndex = this.carditems.length;
      }
    this.pageSlice = this.carditems.slice(startIndex, endIndex);
    }else{
      if (endIndex > this.carditemsFiltred.length) {
        endIndex = this.carditemsFiltred.length;
      }
    this.pageSlice = this.carditemsFiltred.slice(startIndex, endIndex);

    }
  }

  singleRequest(carditemsId : number) {
    this.router.navigate(['/single-request', carditemsId]);
  }
  onClick(){
    this.carditemsFiltred=[]
    this.pageSlice=this.carditems.slice(0, 4);
    
  }
}
function filterByValue(array, id) {
  return array.filter(o => o.TypeId==id);
}