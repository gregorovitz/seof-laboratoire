import { AlertService,AuthService,UserService } from './../services';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  hide = true;
  loading = false;
  registerForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private userService: UserService,
              private router: Router,
              private alertService: AlertService) { 
                 // redirect to home if already logged in
                if (this.authService.currentUserValue) {
                  this.router.navigate(['/home']);
              }
              }
   
  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      confirmPassword:['',[Validators.required]]
    }, {validators:this.checkPasswords});
  }

  onSubmit() {
    this.alertService.clear();
    this.userService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                (data:any) => {
                  console.log(data.message) 
                    if (data.message==="user created"){
                      this.router.navigate(['/login']);
                      this.alertService.success('Registration successfull',true)
                  }else{
                    this.alertService.error(data.message);
                  }
                },
                error => {
                  this.alertService.error(error);
                  this.loading = false;
                });
    }
    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value;
  
    return pass === confirmPass ? null : { notSame: true }     
  }  
}
