import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UpdateOfferComponent } from './offers/update-offer/update-offer.component';
import { SingleOfferComponent } from './offers/single-offer/single-offer.component';
import { SingleRequestComponent } from './requests/single-request/single-request.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ProfileUserComponent } from './profileUser/profileUser.component';
import { OffersComponent } from './offers/offers.component';
import { RequestsComponent } from './requests/requests.component';
import { addServiceComponent } from './addService/addService.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UpdateRequestComponent } from './requests/update-request/update-request.component';
const routes: Routes = [
  { path: ' ', redirectTo: '/home' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'profileUser/:id', component: ProfileUserComponent },
  { path: 'offers', component: OffersComponent },
  { path: 'requests', component: RequestsComponent },
  { path: 'single-offer/:id', component: SingleOfferComponent },
  { path: 'single-request/:id', component: SingleRequestComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'addService', component: addServiceComponent},
  { path: 'user/single-offer/:id',component:UpdateOfferComponent},
  { path: 'user/single-request/:id', component: UpdateRequestComponent},
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
