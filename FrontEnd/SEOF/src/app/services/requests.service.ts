import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private http: HttpClient) { }
  getAll() {
    return this.http.get(`http://localhost:5000/demande`);
}
getOne(id){
  return this.http.get(`http://localhost:5000/demande/${id}`);
}
delete(id){
  return this.http.get(`http://localhost:5000/demande/${id}/delete`);
}
update(service){
  console.log(service)
  return this.http.put(`http://localhost:5000/demande/${service.offreId}/update`,{service});
}
}
  