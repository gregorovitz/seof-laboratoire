import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Offer } from '../models/offer.model';



@Injectable({
  providedIn: 'root'
})
export class OffersService {
  constructor(private http: HttpClient) { }
  getAll() {
    return this.http.get(`http://localhost:5000/offers`);
}
  getOne(id){
    return this.http.get(`http://localhost:5000/offers/${id}`);
  }
  delete(id){
    return this.http.get(`http://localhost:5000/offers/${id}/delete`);
  }
  update(service){
    console.log(service)
    return this.http.put(`http://localhost:5000/offers/${service.offreId}/update`,{service});
  }
}