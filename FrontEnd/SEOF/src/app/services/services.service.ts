import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http: HttpClient) { }
  create(services,idUser) {
    return this.http.post(`http://localhost:5000/user/services/add`, {services:services,userid:idUser});
}
}
