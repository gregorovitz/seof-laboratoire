export class City {
  id: number;
  name: string;
  createdAt: string;
  updatedAt: string;
}