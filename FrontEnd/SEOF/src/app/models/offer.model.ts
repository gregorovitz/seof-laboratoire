export class Offer {
    id: number;
    title: string;
    description: string;
    offer_date: Date;
    price: number;
    updatedAt: Date;
    UserId: number;
    CityId: number;
    TypeId: number;
    User:{
        id:string;
        mail:string;
    }
    City:{
        id:number;
        name:string
    }
    Type:{
        id:number;
        name:string;
    }
}