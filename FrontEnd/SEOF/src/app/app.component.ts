import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from './services/alert.service'; 
import { AuthService } from './services/auth.service';
import { User } from './models';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  currentUser: User;
  constructor(private router: Router,
    private authService: AuthService,
    private alertService: AlertService) {
      this.authService.currentUser.subscribe(x => this.currentUser = x);
    };
    

  logout() {
    this.authService.signOutUser();
    this.router.navigate(['/login']);
}
}

