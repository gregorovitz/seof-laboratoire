import { ServicesService, CategorieService,AlertService,CityService,AuthService} from './../services';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User,City } from '../models'; 

@Component({
  selector: 'app-addService',
  templateUrl: './addService.component.html',
  styleUrls: ['./addService.component.scss']
})
export class addServiceComponent implements OnInit {

  RecordServiceForm: FormGroup;
  errorMessage: string;
  cities=<any>[]
  allCities=[]
  categories:[]
  currentUser:User
  selectService:number
  
  constructor(private formBuilder: FormBuilder,
              private citiesService:CityService,
              private categorieService:CategorieService,
              private alertService: AlertService,
              private servicesService:ServicesService,
              private authService: AuthService) { 
                this.authService.currentUser.subscribe(x => this.currentUser = x);
              }
/**
 * fonction run a l'initialisation de la page 
 * initialise le formulaire
 * initialise la liste des city
 * subcribe au changement dans auto complete pour le filtrage de celui-ci
 * initialise la liste des categorie
*/
  ngOnInit() {            
      this.initForm();   
      this.citiesService.getAll()
      .subscribe((city)=>{
        this.allCities=city
      })  
      this.RecordServiceForm.valueChanges.subscribe(
        term => {
         
          if (term.localite != '') {
            this.cities=filterByValue(this.allCities,term.localite)
          }
      })    
      this.categorieService.getAll()
      .subscribe((categorie)=>{
        this.categories=categorie
      })    
      
  } 
  /**
   * @description modifie la valeur de selectService avec la valeur de event.value
   * @param event  donnée des radioButton
   */
  onItemChange(event){
   
    this.selectService=event.value
    
 }
 /**
  * initialise RecordServiceForm
  */
  initForm() {
    this.RecordServiceForm = this.formBuilder.group({
      selectService: ['', [Validators.required]],
      title: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      description: ['', [Validators.required]],
      categorie: ['', [Validators.required]],
      dateBesoin: ['', [Validators.required]],
      localite: ['', [Validators.required]],
      prix:['']
    });
  }
/**
 * enregistre dans la base de donnée un user
 * if user created => alertService succes
 * else =>alertService error
 */
  onSubmit() {
    this.alertService.clear();
    this.servicesService.create(this.RecordServiceForm.value,this.currentUser.id)
    .pipe()
    .subscribe(
        (data:any) => {
          console.log(data.message) 
            if (data.message==="user created"){
              
              this.alertService.success('Registration successfull',true)
          }else{
            this.alertService.error(data.message); 
          }
        },
        error => {
          this.alertService.error(error);
          
        });
  }
  
 

  
}
/**
 * 
 * @param {Object[]} array -the object array which should be filtered by object name
 * @param {String} term -  the term that must be included in the name of each Object in the array after filter
 * @return {objet[]} - the object array filter by object name
 */
function filterByValue(array, term) {
  return array.filter(o => o.name.toLowerCase().includes(term.toLowerCase()));
}
