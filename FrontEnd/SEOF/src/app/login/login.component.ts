import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService,AlertService } from '../services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);
  hide = true;
  signinForm: FormGroup;
  errorMessage: string;
  returnUrl: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private alertService: AlertService) { 
                // redirect to home if already logged in
        if (this.authService.currentUserValue) {
          this.router.navigate(['/home']);
              }
            }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signinForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }

  onSubmit() {
    const email = this.signinForm.get('email').value;
    const password = this.signinForm.get('password').value;
    this.alertService.clear();
    this.authService.signInUser(this.signinForm.value).pipe(first())
    .subscribe(
      (data:any)=> {
          console.log(data)
          if (data.user){
            this.router.navigate(['/home']);
            this.alertService.success(data.message,true)
          }else{
            this.alertService.error(data.message);
          }
        },
        error => {
          this.alertService.error(error);
            console.log('salut')
        });
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'Champ obligatoire !';
    }
    else if (this.password.hasError('required')) {
      return 'Champ obligatoire !';
    }
  }
}