import Express from 'express';

import user_controller from '../controller/userController.js'
import service_controller from '../controller/serviceController.js'
import cities_controller from '../controller/citiesController.js'
import types_controller from '../controller/typesController.js'

const Router=Express.Router();
Router.post("/users/register",user_controller.user_register);
Router.post("/users/authenticate", user_controller.user_authenticate)
Router.post("/user/services/add",service_controller.service_add)
Router.get("/city",cities_controller.Cities_get_all)
Router.get("/city/init",cities_controller.Cities_init)
Router.get("/type",types_controller.Types_get_all)
Router.get('/offers',service_controller.Offers_get_all)
Router.get('/demande',service_controller.Demande_get_all)
Router.get('/offers/:id',service_controller.Offers_get_one)
Router.get('/demande/:id',service_controller.Demande_get_one)
Router.get("/users/:userId/services",user_controller.user_get_all_service);
Router.get('/offers/:id/delete',service_controller.delete_offers)
Router.get('/demande/:id/delete',service_controller.delete_request)
Router.put('/offers/:id/update',service_controller.update_offers)
Router.put('/demande/:id/update',service_controller.update_request)
export default Router; 