import Sequelize from "sequelize"
export class RoomChat extends Sequelize.Model{
    
    static init (sequelize,DataTypes){
        return super.init({
            
        },{
            sequelize,
            timestamps:true,
            createdAt:true,
            updatedAt:true,
            deletedAt:true,
        }
        
        )
    }
     static associate(models) {
        this.message = this.hasMany(models.Message);
        this.user= this.belongsTo(models.User);
        this.demande =this.belongsTo(models.Demande);
        this.offers=this.belongsTo(models.Offers);
    }
} 