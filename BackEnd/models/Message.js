import Sequelize from "sequelize"
export class Message extends Sequelize.Model{
    
    static init (sequelize,DataTypes){
        return super.init({
            message:{
            type:DataTypes.TEXT,
            allowNull:false,
            }
        },{
            sequelize,
            timestamps:true,
            createdAt:true,
            updatedAt:true,
            
        }
        
        )
    }
     static associate(models) {
        this.roomChat = this.belongsTo(models.RoomChat);
        this.user= this.belongsTo(models.User);
        
    }
} 