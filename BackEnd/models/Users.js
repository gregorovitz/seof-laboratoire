import Sequelize from "sequelize"
export  class Users extends Sequelize.Model{
    mail;
    password;
   
    static init (sequelize,DataTypes){
        return super.init({
            
            mail:{
                type:DataTypes.STRING,
                allowNull:false,
                unique:true,
                validate:{
                    isEmail:true
                }
            },
                password:{
                    type:DataTypes.STRING,
                    allowNull:false
                },
                   
        },{
            sequelize,
            timestamps:true,
            createdAt:true,
            updatedAt:true,
              
        },
        
        )
    }
      
    static associate(models) {
        this.Demande = this.hasMany(models.Demande);
        this.Offers= this.hasMany(models.Offers);
        this.RoomChat=this.hasMany(models.RoomChat);
        this.message=this.hasMany(models.Message)

    }
} 
