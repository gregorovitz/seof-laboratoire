import express from 'express';
import db from './models/index.js';
import cors from 'cors'
import router from './config/router.js'
import bodyParser from 'body-parser';
const server = express();

db.sequelize.sync();
server.use(bodyParser.json()); 
server.use(bodyParser.urlencoded({
  extended: true
}));
server.use(cors());
server.use("/", router);


server.listen(5000, () => console.log("Server lancé sur le port 5000"));