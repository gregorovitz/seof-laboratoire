import { Cities} from '../models/Cities.js';
import{Offers} from'../models/Offers.js';
import{Demande} from'../models/Demande.js';
import db from '../models/index.js'
import { Types } from '../models/Types.js';
import { Users } from '../models/Users.js';
const service_controller={
  
    service_add(req,res){
        let date=new Date(req.body.services.dateBesoin)
        
        const data={
            id: req.body.userid,
            title:req.body.services.title,
            description: req.body.services.description,
            offer_date:date.toISOString().split('T')[0],
            price: req.body.services.prix?req.body.services.prix:null,
            city: req.body.services.localite,
            type:req.body.services.categorie,
            isoffer:req.body.services.selectService,
        };
        Cities.findOne({
            where:{
                name:data.city
            }
        })
        .then(city=>{
            if (city){
                db.sequelize.query(`createService @id_user =${parseInt(data.id)},
                                     @id_city = ${parseInt(city.dataValues.id)}, 
                                     @id_type = ${parseInt(data.type)},
                                     @title = ?, 
                                     @description = ?, 
                                     @offer_date = '${data.offer_date}', 
                                     @price = ${data.price!=null?parseInt(data.price):null}, 
                                     @isoffer = ${data.isoffer}`,
                                     {replacements:[`${data.title}`,`${data.description}`],type:db.sequelize.QueryTypes.INSERT})
                .then(()=>{
                    res.json({message:'service created'});
                })
            }
        }) 
    },
    Offers_get_all(req,res){
        Offers.findAll()
        .then (offer=>{
                        res.jsonp(offer);
                    })
        .catch(err=>{
            console.log('promblem communicating with db');
            res.status(500).json(err);
        })
    },
    Demande_get_all(req,res){
        Demande.findAll()
        .then (demande=>{
                        res.jsonp(demande);
                    })
        .catch(err=>{
            console.log('promblem communicating with db');
            res.status(500).json(err);
        })
    },Offers_get_one({params:{id}},res){
        Offers.findByPk(parseInt(id),{include:[{model:Users},{model:Cities},{model:Types}]})
        .then (offers=>{
                        res.jsonp(offers);
                    })
        .catch(err=>{
            console.log('promblem communicating with db');
            res.status(500).json(err);
        })
    }
    ,Demande_get_one({params:{id}},res){
        Demande.findByPk(parseInt(id),{include:[{model:Users},{model:Cities},{model:Types}]})
        
        .then (demande=>{
                        res.jsonp(demande);
                    })
        .catch(err=>{
            console.log('promblem communicating with db');
            res.status(500).json(err);
        })
    },
    delete_request({params:{id}},res){
         Demande.findByPk(parseInt(id))
         .then(demande=>demande.destroy())
         .then(d=>{
             if (d){
                 res.status(200).jsonp({message:'request service deleted',request:id})
             }
         })
    },
    delete_offers({params:{id}},res){
        Offers.findByPk(parseInt(id))
        .then(offer=>offer.destroy())
        .then(o=>{ 
            if (o){
                 res.status(200).jsonp({message:'offers service deleted',offer:id})
            }
        })
   },
    update_offers(req,res){
        const{title,description,categorie,dateBesoin,localite,prix,userId,offreId}=req.body.service
        console.log(req)
        Offers.findByPk(parseInt(offreId))
        .then(offer=>{ 
            if (offer.dataValues.UserId=userId){
                Cities.findOne({
                    where:{
                        name:localite
                    }
                })
                .then(city=>{
                    Types.findByPk(parseInt(categorie))
                    .then(types=>{
                        offer.update({title:title,description:description,offer_date:dateBesoin,price:parseFloat(prix),CityId:city.dataValues.id,TypeId:types.dataValues.id})
                        .then(up=>res.status(200).jsonp({message:'offer successfull updated'}))
                    })
                })
            }
        console.log(offer)
        })
   },
    update_request(req,res){
        const{title,description,categorie,dateBesoin,localite,userId,offreId}=req.body.service
        console.log(req)
        Demande.findByPk(parseInt(offreId))
        .then(demande=>{ 
            if (demande.dataValues.UserId=userId){
                Cities.findOne({
                    where:{
                        name:localite
                    }
                })
                .then(city=>{
                    Types.findByPk(parseInt(categorie))
                    .then(types=>{
                        demande.update({title:title,description:description,offer_date:dateBesoin,CityId:city.dataValues.id,TypeId:types.dataValues.id})
                        .then(up=>res.status(200).jsonp({message:'offer successfull updated'}))
                    })
                })
         
            }
        console.log(demande)
        })
    }

}
export default service_controller;