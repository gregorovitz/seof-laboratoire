import { Users } from '../models/Users.js';
import config from '../config.json';
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken';
import { Offers } from '../models/Offers.js';
import{Demande} from '../models/Demande.js'
const user_controller={
    user_register(req,res){
        const data={
            mail: req.body.email,
            password:req.body.password
        };
        if (data.password==='' || data.mail ===''){
            res.json({message:'username and password required'})
        } 
        Users.findOne({
            where:{
                mail:data.mail
            }
        })
        .then (user=>{
            if (user!= null){
                res.json({message:'email already taken'});
            }else{
                bcrypt.hash(data.password ,10, (err, hash) => {
                    Users.create({
                        mail:data.mail,
                        password:hash
                    }).then(()=>{
                        res.json({message:'user created'});
                    })
                });
            }
        })
        .catch(err=>{
            console.log('promblem communicating with db');
            res.status(500).json(err);
        })
    },
    user_authenticate(req,res){
        const data={
            mail: req.body.email, 
            password:req.body.password
        };
        Users.findOne({
            where:{
                mail:data.mail
            }
        })
        .then (user=>{
            console.log(user)
            
            if (user){
                if(bcrypt.compareSync(data.password, user.dataValues.password)){
                const token = jwt.sign({ sub: user.dataValues.id }, config.secret, { expiresIn: '7d' });
                
                
                res.json({
                    message:'successfull connection',
                    user: {id:user.dataValues.id,email: user.dataValues.mail,password:user.dataValues.password,token:token}
                   
                });
                }else{
                    res.json({message:'incorrect password'});
                }
            }else {
                res.json({message:'no user with this email'})
            }
        })
        .catch(err=>{
            console.log('promblem communicating with db');
            res.status(500).json(err);
        })
    },
    user_get_all_service({params: {userId}},res){
        
        Users.findByPk(parseInt(userId),{include:[{model:Offers},{model:Demande}]})
        .then(user=>{
                    res.status(200).jsonp(user)
        })
        .catch(err=>{
            console.log('promblem communicating with db');
            res.status(500).json(err);
        })
    }
}
export default user_controller